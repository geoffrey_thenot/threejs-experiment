var Wing = (function(){

    function Wing(size, rotate){
        THREE.Object3D.call(this);

        var rotate = rotate || 1;
        var size = size || 1;

        var v1 = new THREE.Vector3(0, 0, 0);
        var v2 = new THREE.Vector3(100 * size , 0, 0);
        var v3 = new THREE.Vector3(0, 60 * rotate * size, 0);
        var geometry = new THREE.Geometry();
        geometry.vertices.push(v1, v2, v3);

        geometry.faces.push(new THREE.Face3(0, 2, 1));

        var material = new THREE.MeshBasicMaterial({color: 0x3facc8, wireframe: false, side: THREE.DoubleSide});
        material;
        this.mesh = new THREE.Mesh(geometry, material);
        this.add(this.mesh);
        this.direction = 1;
    }

    Wing.prototype = new THREE.Object3D;
    Wing.prototype.constructor = Wing;

    return Wing;
})();
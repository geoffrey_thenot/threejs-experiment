var Particule = (function(){

    function Particule(){
        THREE.Object3D.call(this);

        var v1 = new THREE.Vector3(0, 0, 0);
        var v2 = new THREE.Vector3(100, 0, 0);
        var v3 = new THREE.Vector3(0, 100, 0);
        var geometry = new THREE.Geometry();
        geometry.vertices.push(v1, v2, v3);

        geometry.faces.push(new THREE.Face3(0, 2, 1));

        var material = new THREE.MeshBasicMaterial({color: 0x3facc8, wireframe: true});
        this.mesh = new THREE.Mesh(geometry, material);
        this.add(this.mesh);
    }

    Particule.prototype = new THREE.Object3D;
    Particule.prototype.constructor = Particule;

    Particule.prototype.update = function() {
        this.mesh.rotation.y += 0.01;
    };

    return Particule;
})();
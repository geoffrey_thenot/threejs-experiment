var Bird = (function(){

    function Bird(){
        THREE.Object3D.call(this);

        this.leftWing = new Wing(1, 1);
        this.rightWing = new Wing(1, 1);

        this.add(this.leftWing);
        this.add(this.rightWing);

        var v1 = new THREE.Vector3(130, 10, 0);
        var v2 = new THREE.Vector3(100, 0, 0);
        var v3 = new THREE.Vector3(150, -20, 0);


        var geometry = new THREE.Geometry();
        geometry.vertices.push(v1, v2, v3);
        geometry.faces.push(new THREE.Face3(0, 2, 1));

        var material = new THREE.MeshBasicMaterial({color: 0x3facc8, wireframe: false, side: THREE.DoubleSide});

        this.head = new THREE.Mesh(geometry, material);
        this.add(this.head);


        this.direction = 1;
        this.velocity = 0.05;
        this.velocityV = 1;

        this.flappy = 0;

        this.hightFlap = false;

        this.isMovingUp = false;

        this.count = 0;
        this.boostVelocity = false;
    }

    Bird.prototype = new THREE.Object3D;
    Bird.prototype.constructor = Bird;

    Bird.prototype.move = function() {
        var y = this.position.y;

        if (this.flappy > 2.5 || this.flappy < 0) {
            this.direction *= -1;
            if (this.direction < 0) {
                this.isMovingUp = true;
            }
        }

        this.flappy += ((this.velocity * this.velocityV) * this.direction);

        this.leftWing.rotation.x = this.flappy;
        this.rightWing.rotation.x = -this.flappy;

        if (this.velocityV === 1) {
            this.translateY(-0.4);
        }

        this.moveUp();

        if (this.position.y < -150) {
            this.velocityV = 6;
        } else if (this.position.y > 150) {
            this.velocityV = 1;
        }

    };

    Bird.prototype.moveUp = function() {
        if (this.isMovingUp && this.count < 30) {
            this.count++;
            this.translateY(1);
        } else {
            this.isMovingUp = false;
            this.count = 0;
        }
    };

    return Bird;
})();
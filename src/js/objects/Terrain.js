var Terrain = (function(){

    function Terrain(){
        THREE.Object3D.call(this);

        this.worldWidth = this.worldDepth = 64;

        this.data = this.generateHeight(this.worldWidth, this.worldDepth);

        var geometry = new THREE.PlaneGeometry(16000, 8000, this.worldWidth - 1, this.worldDepth - 1);
        for(var i = 0, l = geometry.vertices.length; i < l; i++) {
            geometry.vertices[i].z = this.data[i] * 40;
        }

        var material = new THREE.MeshBasicMaterial({color: 0x757a7f, wireframe: true, side: THREE.DoubleSide});

        this.mesh = new THREE.Mesh(geometry, material);
        this.mesh.rotation.x = -Math.PI * 0.5;
        this.mesh.position.y = -1700;
        this.mesh.position.z = -3500;
        this.add(this.mesh);
    }

    Terrain.prototype = new THREE.Object3D;
    Terrain.prototype.constructor = Terrain;

    Terrain.prototype.generateHeight = function(width, height) {
        var size = width * height,
            data = new Uint8Array(size),
            perlin = new ImprovedNoise(),
            quality = 1,
            iterations = 4,
            z = 19.9,
            x, y;

        for(var j = 0; j < iterations; j++) {
            for (var i = 0; i < size; i++) {
                x = i % width;
                y = ~~(i / width);
                data[i] += Math.abs(perlin.noise(x / quality, y / quality, z) * quality);
            }
            quality *= 5;
        }

        return data;
    };

    return Terrain;
})();
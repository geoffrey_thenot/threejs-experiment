var Webgl = (function(){

    function Webgl(width, height) {
        this.ambientColor = [ 0, 0, 0 ];

        // Basic three.js setup
        this.scene = new THREE.Scene();
        
        this.camera = new THREE.PerspectiveCamera(50, width / height, 1, 10000);
        this.camera.position.z = 500;

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize(width, height);
        this.renderer.setClearColor(0x2D2D2D);


        // Trackball
        this.controls = new THREE.TrackballControls( this.camera );

        this.controls.rotateSpeed = 1.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
        this.controls.noZoom = false;
        this.controls.noPan = false;
        this.controls.staticMoving = true;
        this.controls.dynamicDampingFactor = 0.3;

        this.controls.keys = [ 65, 83, 68 ];

        light = new THREE.DirectionalLight( 0xffffff );
        light.position.set( 0, 1, 0 );
        this.scene.add( light );

        // this.ambientLight = new THREE.AmbientLight(0x000000);
        // this.ambientLight.color.setRGB(this.ambientColor[0] / 255, this.ambientColor[1] / 255, this.ambientColor[2] / 255);
        // this.scene.add(this.ambientLight);

        // Terrain
        this.terrain = new Terrain();
        this.scene.add(this.terrain);

        // Add a bird
        this.bird = new Bird();
        this.bird.position.set(10, 0, 0);
        this.scene.add(this.bird);

    };

    Webgl.prototype.resize = function(width, height) {
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    };

    Webgl.prototype.render = function() {    
        this.renderer.render(this.scene, this.camera);
        this.controls.update();

        this.bird.move();
        this.bird.rotation.y += 0.01;
        //this.bird.rotation.x += 0.01;
    };

    return Webgl;

})();